package com.qovn.demo.week.two.nio;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// 创建固定的大小的线程池进行请求
public class HttpServer03 {
    public static void main(String[] args) throws IOException {
        //创建一个线程池，该线程池重用固定数量的线程在共享的无界队列中运行
        ExecutorService executorService= Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*4);
        ServerSocket serverSocket=new ServerSocket(8803);
        while (true){
            try {
               final Socket socket=serverSocket.accept();
               executorService.execute(()->
                   service(socket));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void service(Socket socket){
        try {
            PrintWriter printWriter=new PrintWriter(socket.getOutputStream(),true);
            printWriter.println("HTTP/1.1 200 0K");
            printWriter.println("Content-Type:text/html;charset=utf-8");
            String body="Hello,nio3";
            printWriter.println("Content-length:"+body.getBytes().length);//说明报文体多长
            //空行前报文头 空行后报文体
            printWriter.println();
            printWriter.println(body);
            printWriter.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
