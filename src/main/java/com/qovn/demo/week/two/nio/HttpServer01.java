package com.qovn.demo.week.two.nio;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 单线程socket
 */
public class HttpServer01 {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket=new ServerSocket(8801);
        while (true){
            try {
                Socket socket=serverSocket.accept();
                service(socket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void service(Socket socket){
        try {
            PrintWriter printWriter=new PrintWriter(socket.getOutputStream(),true);
            printWriter.println("HTTP/1.1 200 0K");
            printWriter.println("Content-Type:text/html;charset=utf-8");
            String body="Hello,nio1";
            printWriter.println("Content-length:"+body.getBytes().length);//说明报文体多长
            //空行前报文头 空行后报文体
            printWriter.println();
            printWriter.println(body);
            printWriter.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
