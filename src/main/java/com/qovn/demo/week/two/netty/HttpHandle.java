package com.qovn.demo.week.two.netty;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;

import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaderValues.KEEP_ALIVE;
import static io.netty.handler.codec.http.HttpResponseStatus.NO_CONTENT;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

// Netty 启动成功 当客户端请求进来 我们读取客户端请求的这个Handler
public class HttpHandle extends ChannelInboundHandlerAdapter {
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
//        super.channelReadComplete(ctx);
        ctx.flush();
    }

    @Override //msg  本身代表数据请求的所有包装类的对象
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
        FullHttpRequest fullHttpRequest=(FullHttpRequest)msg;
        String uri=fullHttpRequest.uri();
        if (uri.contains("/test")){
            handTest(fullHttpRequest,ctx,"test 进入");
        }else {
            handTest(fullHttpRequest,ctx,"test 没有 进入");
        }
    }

    private void handTest(FullHttpRequest fullHttpRequest, ChannelHandlerContext ctx,String msg) {
        FullHttpResponse response=null;
        try {
            String value=msg;
            response=new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.wrappedBuffer(value.getBytes("UTF-8")));
            response.headers().set("Content-Type","application/json");
            response.headers().setInt("Content-length",response.content().readableBytes());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            response=new DefaultFullHttpResponse(HTTP_1_1,NO_CONTENT);
        } finally {
            if (fullHttpRequest != null){
                if (!HttpUtil.isKeepAlive(fullHttpRequest)){
                    ctx.write(response).addListener(ChannelFutureListener.CLOSE);
                }else {
//                    response.headers().set("CONNECTION","KEEP_ALIVE");
                    response.headers().set(CONNECTION,KEEP_ALIVE);
                    ctx.write(response);

                }
            }
        }

    }


}
