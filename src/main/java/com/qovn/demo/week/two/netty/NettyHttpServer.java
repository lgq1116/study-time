package com.qovn.demo.week.two.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.channel.nio.NioEventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class NettyHttpServer {
    public static void main(String[] args) {
        int port = 8808;
        EventLoopGroup bossGroup= new NioEventLoopGroup(2);//创建线程 指定实例
        EventLoopGroup workGroup= new NioEventLoopGroup(16);
        try {

            // ServerBootstrap -> Bootstrap子类，它允许轻松引导ServerChannel
            // 作为启动整个NettyHttpServer 的入口点
            ServerBootstrap b=new ServerBootstrap();
            //允许指定一个ChannelOption ，一旦它们被创建， ChannelOption用于Channel实
            b.option(ChannelOption.SO_BACKLOG,128)
                    //允许指定一个ChannelOption ，一旦它们被创建（在接受者接受Channel ）， ChannelOption用于Channel实例
                    .childOption(ChannelOption.TCP_NODELAY,true)//TCP节点延迟
                    .childOption(ChannelOption.SO_KEEPALIVE,true)//保持活力（动态）
                    .childOption(ChannelOption.SO_REUSEADDR,true)//重用地址
                    .childOption(ChannelOption.SO_RCVBUF,32*1024)
                    .childOption(ChannelOption.SO_SNDBUF,32*1024)
                    .childOption(EpollChannelOption.SO_REUSEPORT,true)//重用地址
                    .childOption(ChannelOption.SO_KEEPALIVE,true)
                    .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT); //分配器

            b.group(bossGroup,workGroup).channel(NioServerSocketChannel.class)
                .handler(new LoggingHandler(LogLevel.INFO)).childHandler(new HttpInitializer());
            // 绑定端口 开启它的channel
            Channel ch=b.bind(port).sync().channel();
            System.out.println(" 开启Netty http 服务器 ，监听地址和端口为http://127.0.0.1:"+port+'/' );
            ch.closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //具有合理默认值的shutdownGracefully(long, long, TimeUnit)快捷方法
            bossGroup.shutdownGracefully();
            workGroup.shutdownGracefully();
        }
    }
}
