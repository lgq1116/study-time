package com.qovn.demo.week.one;


import sun.misc.Launcher;

import java.net.URL;

public class JvmClassLoaderPrintPath {
    public static void main(String[] args) {
        //启动类加载器
        URL[]  urls= Launcher.getBootstrapClassPath().getURLs();
        System.out.println(" 启动类加载器" );
        for (URL url : urls) {
            System.out.println("url.toExternalForm() = " + url.toExternalForm());
        }



    }

    private static void printClassLoader(String name, HelloClassloader classloader){
        System.out.printf("");
        if (null != classloader){
            System.out.println("name = " +name+"Classloader -> "+classloader.toString());

        }
        else {
            System.out.println("name = " + name+"Classloader ->null");
        }

    }
}
