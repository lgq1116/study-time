package com.qovn.demo.week.one;



import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.io.resource.ResourceUtil;
import sun.management.MethodInfo;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Base64;

public class HelloClassloader extends ClassLoader {

    static String path=System.getProperty("user.dir")+ File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator;
    static String class_suffix=".class";
    static String class_suffix_=".xlass";

    public static void main(String[] args) throws Exception{
       new HelloClassloader().findClass("Hello").newInstance();
        Class aClass = Class.forName("Hello");
        Object object=aClass.newInstance();
// 调用里面的方法
        Method method1= aClass.getDeclaredMethod("hello", aClass.getClasses()) ;
        method1.invoke(object,aClass.getClasses());

    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] bytes=ResourceUtil.readBytes(name+class_suffix_);
        byte[] bytes_opposite = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            bytes_opposite[i]=(byte)(255-bytes[i]);
        }
        //写回去 主要是想看看能不能成功
        FileWriter writer = new FileWriter(path+name+class_suffix);
        System.out.println(name+class_suffix+" 文件生成成功 " );
        System.out.println(" 文件路径= " +path+name+class_suffix);
        writer.write(bytes_opposite,0,bytes_opposite.length);
        return defineClass(name,bytes_opposite,0,bytes_opposite.length);
    }

    public byte[] decode(String base64){
        return Base64.getDecoder().decode(base64);
    }
}
