package com.qovn.demo;

import com.qovn.demo.week.four.TestFour;
import com.qovn.demo.week.one.HelloClassloader;
import com.thebeastshop.forest.springboot.annotation.ForestScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@ForestScan(basePackages="com.qovn.demo.week.two.nio.dao")
public class DemoApplication {

    public static void main(String[] args) throws Exception{
        SpringApplication.run(DemoApplication.class, args);
        new TestFour().TestThread_1();

    }

}
